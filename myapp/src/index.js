import React from 'react';
import {render} from 'react-dom';

import store from './redux/store';
import {Provider} from 'react-redux';
import './index.css';
import {BrowserRouter} from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import AppRouter from "./routers/appRouter";



render(
    <Provider store={store}>
        <BrowserRouter>
        <AppRouter/>
        </BrowserRouter>
    </Provider>

    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
