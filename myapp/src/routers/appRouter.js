import React,{Component} from 'react';

import store from  './../redux/store';
import {Route,Switch } from 'react-router-dom';
import Product from './../components/product';
import './../App.css';
import AddProduct from './../components/addProduct';
import Header from "../components/Header";

class AppRouter extends Component {


    render() {

        return (
           <div>


                <Header/>

                    <Switch>
                        <Route path="/home"  exact={true} />
                        <Route path="/product" component={Product} />
                        <Route path="/basket" component={AddProduct} />

                    </Switch>
           </div>


                );




    }
}

export default AppRouter;




