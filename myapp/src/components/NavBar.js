import React from 'react';
import {Navbar,Nav,NavItem} from 'react-bootstrap';
import {Link} from 'react-router-dom';

class NavBar extends React.Component{
    render(){
        return(
            <div>
                <Navbar className={"navBar"}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to={"/home"}>صفحه اصلی</Link>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                        <NavItem eventKey={1} >
                            <Link  to={"/product"}>لوازم جانبی کامپیوتر</Link>
                        </NavItem>
                        {/*<NavItem eventKey={2} >*/}
                            {/*<Link  to={"/basket"}>کامپیوتر</Link>*/}
                        {/*</NavItem>*/}

                    </Nav>
                </Navbar>
            </div>
        );
    }
}

export default NavBar;