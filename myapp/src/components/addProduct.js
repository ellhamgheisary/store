import store from './../redux/store';
import ClearProduct from './../redux/action/clearproduct';
import {connect} from 'react-redux';
import DecreaseCounter from "../redux/action/decreasecounter";
import React, {Component} from 'react';
import {Button} from "react-bootstrap";


class AddProduct extends React.Component {


    handleClick(id, counter) {
        this.props.dispatch(ClearProduct(id));
        this.props.dispatch(DecreaseCounter());


    }

    render() {

        return (
            <div>
                <ul>
                    {this.props.Product.map((item, index) =>
                        <li className={"li"} key={index}>
                            <div className={"and"}>

                                <img className={"img"} src={item.img}/>
                                <p className={"title"}>{item.name}</p>
                                <p className={"text"}>{item.price}</p>

                                <Button className={"buttonP"} onClick={() => this.handleClick(item.id)}> حذف
                                    خرید</Button>


                            </div>
                        </li>
                    )

                    }

                </ul>

            </div>
        );
    }

}
const mapStateToProps = (state) => {
    return {
        Product: state.Product
    };
}


export default connect(mapStateToProps)(AddProduct);