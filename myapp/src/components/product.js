import React from 'react';
import AddProduct from './../redux/action/AddProduct';
import {connect} from 'react-redux';
import store from './../redux/store';
import Counter from './../redux/action/Counter';
import {Container, Row, Col} from 'reactstrap';
import idGenerator from 'react-id-generator';
import {Button} from 'reactstrap';
import NumbersConvertor from './../numbers';
import axios from 'axios';


class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        //     test: 'iii',
           counter: 0,
            
            product: [
                {
                    id: 1,
                    name: 'هارددیسک اینترنال وسترن دیجیتال مدل Blue WD10EZEX ظرفیت 1 ترابایت',
                    price: ' 300000  ',
                    count: 15,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/1808067.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60'
                },
                {
                    id: idGenerator(),
                    name: 'کارت حافظه microSDHC سن دیسک مدل Ultra A1 کلاس 10 استاندارد UHS-I سرعت 98MBps ظرفیت 32 گیگابایت به همراه آداپتور SD',
                    price: 30000,
                    count: 15,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/2326879.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'هدفون بی سیم نیا مدل Q8-851S',
                    price: ' 3400000 ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/1100228.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'هارد اکسترنال ای دیتا مدل HD710 ظرفیت 1 ترابایت',
                    price: ' 300000  ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/281137.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'هدفون بی سیم بیتس مدل Studio 3',
                    price: ' 300000  ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/1979820.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'مودم روتر ADSL2 Plus بی‌ سیم N300 دی-لینک مدل DSL-2740U',
                    price: ' 300000  ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/134822.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'هندزفری بلوتوث مدل HBQ-I7',
                    price: ' 300000 تومان ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/2324935.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                    id: idGenerator(),
                    name: 'اسپیکر بلوتوثی قابل حمل جی بی ال مدل Flip 4',
                    price: ' 300000  ',
                    count: 20,
                    amount: 0,
                    img: 'https://dkstatics-public.digikala.com/digikala-products/1739881.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                },
                {
                     id: idGenerator(),
                     name: 'هارد دیسک اکسترنال توشیبا مدل Canvio Basics ظرفیت 2 ترابایت',
                     price: ' 300000 ',
                     count: 20,
                     amount: 0,
                     img: 'https://dkstatics-public.digikala.com/digikala-products/1572459.jpg?x-oss-process=image/resize,m_lfit,h_500,w_500/quality,q_80'
                 },

             ]
         }
    }

    handleClick(item, counters) {


        this.props.dispatch(AddProduct(item));

        this.props.dispatch(Counter(counters));


    }

    render() {

        return (
            <div>
                <ul className={"ul"}>
                    {this.state.product.map((item, index) =>


                        <li className={"li"} key={index}>
                            <div className={"and"}>


                                <img className={"img"} src={item.img}/>

                                <p className={"title"}>{item.name}</p>
                                <p className={"text"}>{NumbersConvertor().convertToPersian(item.price)} تومان</p>
                                <p className={"count"}>تعداد موجودی
                                    :{NumbersConvertor().convertToPersian(item.count)}</p>
                                <Button className={"buttonP"}
                                        onClick={ ()=>this.handleClick(item, this.state.counter)}>Add To My
                                    Bascket</Button>


                            </div>
                        </li>
                    )}
                </ul>
            </div>);
    }

}

const mapStateToProps = (state) => {
    return {
        Product: state.Product,
        Counter: state.Counter
    };
}
// const mapDispatchToProps=(dispatch,item,counters)=>{
//      handleClich:()=>dispatch(AddProduct(item))
//     handleClich:()=>dispatch(Counter(counters))
// }

export default connect(mapStateToProps)(Product);
