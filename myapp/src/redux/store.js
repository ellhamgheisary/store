import { createStore } from "redux";
import reducer from './../redux/reducers/reducers';
const store = createStore(reducer);
export default store;
