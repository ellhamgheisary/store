import {combineReducers} from 'redux';
import  Product from './products';
import Counter from './Counter'

const reducer=combineReducers({Product,Counter});
export default reducer;