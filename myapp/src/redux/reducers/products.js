const Product=(state=[],action)=>{
    switch(action.type){
        case 'ADD-PRODUCT':

            return [...state,action.items];
        case 'CLEAR-PRODUCT':
            return state.filter(({id}) => id !== action.id);


        default:
            return state;


    }

}
export default Product;